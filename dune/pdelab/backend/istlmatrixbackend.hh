// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_PDELAB_BACKEND_ISTLMATRIXBACKEND_HH
#warning "The file dune/pdelab/backend/istlmatrixbackend.hh is deprecated. Please use dune/pdelab/backend/istl/istlmatrixbackend.hh instead."
#include <dune/pdelab/backend/istl/istlmatrixbackend.hh>
#endif // DUNE_PDELAB_BACKEND_ISTLMATRIXBACKEND_HH
